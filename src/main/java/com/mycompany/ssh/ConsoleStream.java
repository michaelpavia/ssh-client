package com.mycompany.ssh;


import java.io.IOException;
import java.io.OutputStream;
import javafx.scene.control.TextArea;

public class ConsoleStream extends OutputStream {
    
    private TextArea output;

    public ConsoleStream(TextArea ta)
    {
        output = ta;
    }

    @Override
    public void write(int i) throws IOException
    {
        javafx.application.Platform.runLater( () -> {
            output.appendText(String.valueOf((char) i));
            output.end();
        });
    }

}