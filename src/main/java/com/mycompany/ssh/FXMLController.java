package com.mycompany.ssh;

import com.jcraft.jsch.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.nio.file.FileSystem;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.*;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class FXMLController implements Initializable {
    
    @FXML
    private TextField host;
    @FXML
    private TextField port;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label loginError;
    @FXML
    private ComboBox selectProtocol;

    @FXML
    private void chooseLogin(ActionEvent event) throws IOException {
        String selected = (String) selectProtocol.getValue();
        switch (selected) {
            case "FTP":
                loginFTP(event);
                break;
            case "SFTP":
                loginSFTP(event);
                break;
            case "SSH":
                loginSSH(event);
                break;
        }
    }
    
    private void loginFTP(ActionEvent event) throws IOException {
        //ExpandedListener el = new ExpandedListener();
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/FTP.fxml"));
        Scene main = new Scene(root);
        stage.setOnCloseRequest((WindowEvent t) -> {
            Platform.exit();
            System.exit(0);
        });
        stage.setScene(main);
        TreeView localTree = (TreeView)root.getChildrenUnmodifiable().get(0);
        TreeItem<File> [] treeRoots = buildFileSystemBrowser((new File("")).listRoots());
        TreeItem tRoot = new TreeItem();
        ListView localList = (ListView)root.getChildrenUnmodifiable().get(2);
        localTree.setRoot(tRoot);
        localTree.setShowRoot(false);
        localTree.getRoot().getChildren().addAll((Object[]) treeRoots);
        localTree.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {            
                if(mouseEvent.getClickCount() == 2)
                {
                    TreeItem item = (TreeItem) localTree.getSelectionModel().getSelectedItem();
                    String path = "";
                    while(item.getParent() != null) {
                        path = item.getValue().toString() + "\\" + path;
                        item = item.getParent();
                    }
                    File f = new File(path);
                    if (f.isDirectory()) {
                        File[] files = f.listRoots();
                        ObservableList<String> children = FXCollections.observableArrayList();
                        if(files != null) {
                            for (File childFile : files) {
                                if(childFile.isDirectory())
                                    children.add(childFile.getPath());
                            }

                            localList.setItems(children);
                        }
                    }
                }
            }
        });
        main.getStylesheets().add("/styles/Styles.css");
        /*Platform.runLater(() -> {
            TreeItem ti = new TreeItem(new File("C:\\").getName());
            localTree.setRoot(ti);
            localTree.getRoot().expandedProperty().addListener(el);
        });*/
        FTPClient ftp = new FTPClient();
        ftp.connect(host.getText(), Integer.parseInt(port.getText()));
        ftp.login(username.getText(), password.getText());
        TreeView ftpTree = (TreeView)root.getChildrenUnmodifiable().get(1);
        tRoot = new TreeItem();
        ftpTree.setRoot(tRoot);
        ftpTree.setShowRoot(false);
        //treeRoots = buildRemoteFileSystemBrowser(ftp.listDirectories());
        //ftpTree.getRoot().getChildren().addAll((Object[]) treeRoots);
    }
    
    private TreeItem [] buildFileSystemBrowser(File [] roots) {
        TreeItem<File> [] treeRoots = new TreeItem[roots.length];
        for(int i = 0; i < roots.length; i++)
            treeRoots[i] = createNode(roots[i]);
        return treeRoots;
    }

    /*public class File extends File {  

        public File(String pathname) {
            super(pathname);
        }

        public File[] listRoots() {
            File [] roots = listRoots();
            File [] troots = new File[roots.length];
            for(int i = 0; i < roots.length; i++)
                troots[i] = new File(roots[i].toString());
            return troots;
        }
        
        @Override public String toString() {
            return this.getPath();
        }
        
    }*/
    
    // Taken from TreeItem API and heavily modified.
    // This method creates a TreeItem to represent the given File. It does this
    // by overriding the TreeItem.getChildren() and TreeItem.isLeaf() methods 
    // anonymously, but this could be better abstracted by creating a 
    // 'FileTreeItem' subclass of TreeItem. However, this is left as an exercise
    // for the reader.
    private TreeItem<File> createNode(final File f) {
        return new TreeItem<File>(f) {
            // We cache whether the File is a leaf or not. A File is a leaf if
            // it is not a directory and does not have any files contained within
            // it. We cache this as isLeaf() is called often, and doing the 
            // actual check on File is expensive.
            private boolean isLeaf;

            // We do the children and leaf testing only once, and then set these
            // booleans to false so that we do not check again during this
            // run. A more complete implementation may need to handle more 
            // dynamic file system situations (such as where a folder has files
            // added after the TreeView is shown). Again, this is left as an
            // exercise for the reader.
            private boolean isFirstTimeChildren = true;
            private boolean isFirstTimeLeaf = true;
            
            @Override public ObservableList<TreeItem<File>> getChildren() {
                if (isFirstTimeChildren) {
                    isFirstTimeChildren = false;

                    /*ObservableList<TreeItem<File>> nameList = FXCollections.observableArrayList();
                    TreeItem item = this;
                    String path = item.getValue().toString();
                    item = item.getParent();
                    while(item.getParent() != null) {
                        path = item.getValue().toString() + "\\" + path;
                        item = item.getParent();
                    }
                    System.out.println(path);
                    TreeItem<File> f = new TreeItem(new File(path));
                    buildChildren(f).stream().forEach((t) -> { 
                        nameList.add(new TreeItem(t.getValue().getName()));
                    });
                    System.out.println(f + ", " + f.getValue());*/
                    super.getChildren().setAll(buildChildren(this));
                    /*for(int i = 0; i < nameList.size(); i++)
                        System.out.println(nameList.get(i));*/
                    //super.getChildren().setAll(nameList);
                }
                return super.getChildren();
            }

            @Override public boolean isLeaf() {
                if (isFirstTimeLeaf) {
                    isFirstTimeLeaf = false;
                    TreeItem item = this;
                    String path = item.getValue().toString();
                    item = item.getParent();
                    while(item.getParent() != null) {
                        path = item.getValue().toString() + "\\" + path;
                        item = item.getParent();
                    }
                    System.out.println(f.isFile());
                    System.out.println(f.isDirectory());
                    System.out.println(this);
                    System.out.println(path);
                    File f = new File(path);
                    isLeaf = this.getValue().isFile();
                }

                return isLeaf;
            }

            private ObservableList<TreeItem<File>> buildChildren(TreeItem<File> TreeItem) {
                File f = TreeItem.getValue();
                if (f != null && f.isDirectory()) {
                    File[] files = f.listFiles();
                    if (files != null) {
                        ObservableList<TreeItem<File>> children = FXCollections.observableArrayList();

                        for (File childFile : files) {
                            if(childFile.isDirectory())
                                children.add(createNode(childFile));
                        }

                        return children;
                    }
                }

                return FXCollections.emptyObservableList();
            }
        };
    }

    /*private TreeItem [] buildLocalFileSystemBrowser(File [] roots) {
        TreeItem<String> [] treeRoots = new TreeItem[roots.length];
        for(int i = 0; i < roots.length; i++)
            treeRoots[i] = createLocalNode(roots[i].getPath());
        return treeRoots;
    }

    private TreeItem<String> createLocalNode(final String f) {
        return new TreeItem<String>(f) {
            // We cache whether the File is a leaf or not. A File is a leaf if
            // it is not a directory and does not have any files contained within
            // it. We cache this as isLeaf() is called often, and doing the 
            // actual check on File is expensive.
            private boolean isLeaf;

            // We do the children and leaf testing only once, and then set these
            // booleans to false so that we do not check again during this
            // run. A more complete implementation may need to handle more 
            // dynamic file system situations (such as where a folder has files
            // added after the TreeView is shown). Again, this is left as an
            // exercise for the reader.
            private boolean isFirstTimeChildren = true;
            private boolean isFirstTimeLeaf = true;

            @Override public ObservableList<TreeItem<String>> getChildren() {
                if (isFirstTimeChildren) {
                    isFirstTimeChildren = false;

                    // First getChildren() call, so we actually go off and 
                    // determine the children of the File contained in this TreeItem.
                    ObservableList<TreeItem<String>> nameList = FXCollections.observableArrayList();
                    for(TreeItem t : buildChildren(this)) {
                        nameList.add(new TreeItem(((File)t.getValue()).getName()));
                    }
                    super.getChildren().setAll(nameList);
                }
                return super.getChildren();
            }

            @Override public boolean isLeaf() {
                if (isFirstTimeLeaf) {
                    isFirstTimeLeaf = false;
                    File f = new File(getValue());
                    isLeaf = f.isFile();
                }

                return isLeaf;
            }

            private ObservableList<TreeItem<File>> buildChildren(TreeItem<String> TreeItem) {
                File f = new File(TreeItem.getValue());
                if (f.isDirectory()) {
                    File[] files = f.listFiles();
                    if (files != null) {
                        ObservableList<TreeItem<File>> children = FXCollections.observableArrayList();

                        for (File childFile : files) {
                            if(childFile.isDirectory()) {
                                System.out.println(childFile.getPath());
                                createLocalNode(childFile.getPath());
                                TreeItem<File> cF = new TreeItem(childFile);
                                children.add(cF);
                            }
                        }

                        return children;
                    }
                }

                return FXCollections.emptyObservableList();
            }
        };
    }*/
    
    private void loginSFTP(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/SFTP.fxml"));
        Scene main = new Scene(root);
        stage.setOnCloseRequest((WindowEvent t) -> {
            Platform.exit();
            System.exit(0);
        });
        TextArea console = (TextArea)root.getChildrenUnmodifiable().get(0);
        main.getStylesheets().add("/styles/Styles.css");
    }
    
    private void loginSSH(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/SSH.fxml"));
        Scene main = new Scene(root);
        stage.setOnCloseRequest((WindowEvent t) -> {
            Platform.exit();
            System.exit(0);
        });
        TextArea console = (TextArea)root.getChildrenUnmodifiable().get(0);
        main.getStylesheets().add("/styles/Styles.css");
        JSch jsch = new JSch();
        try {
            Session session = jsch.getSession(username.getText(), host.getText(), Integer.parseInt(port.getText()));
            session.setPassword(password.getText());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(30000);
            stage.setScene(main);
            InputStream is = new InputStream() {

                @Override
                public int read() throws IOException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
            InputStream in = new PipedInputStream();
            PipedOutputStream pin = new PipedOutputStream((PipedInputStream) in);
            /**...*/
            ConsoleStream cs = new ConsoleStream(console);
            PrintStream ps = new PrintStream(cs, true);
            Channel channel = session.openChannel("shell");
            channel.setInputStream(in);
            channel.setOutputStream(ps);
            channel.connect(3*1000);
            console.addEventFilter(KeyEvent.KEY_TYPED, (KeyEvent e) -> {
                TextArea tf = (TextArea) e.getSource();
                System.out.println(e.getCharacter());
                
                if((tf.getText().lastIndexOf("$") != -1 && tf.caretPositionProperty().getValue() <= tf.getText().lastIndexOf("$") + 1) || (tf.getText().lastIndexOf(">") != -1 && tf.caretPositionProperty().getValue() <= tf.getText().lastIndexOf(">"))) {
                    e.consume();
                }
            });
            console.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent e) -> {
                TextArea tf = (TextArea) e.getSource();
                
                if((tf.getText().lastIndexOf("$") != -1 && tf.caretPositionProperty().getValue() <= tf.getText().lastIndexOf("$") + 2) || (tf.getText().lastIndexOf(">") != -1 && tf.caretPositionProperty().getValue() <= tf.getText().lastIndexOf(">") + 1)) {
                    e.consume();
                }
            });
            console.addEventFilter(KeyEvent.KEY_RELEASED, (KeyEvent e) -> {
                TextArea tf = (TextArea) e.getSource();
                
                if(e.getCode().equals(KeyCode.ENTER)) {
                    if(tf.getText().lastIndexOf("$") != -1) {
                        try {
                            e.consume();
                            pin.write((tf.getText().substring(tf.getText().lastIndexOf("$") + 2).getBytes()));
                            pin.flush();
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else if(tf.getText().lastIndexOf(">") != -1) {
                        try {
                            e.consume();
                            pin.write((tf.getText().substring(tf.getText().lastIndexOf(">") + 1).getBytes()));
                            pin.flush();
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        } catch (JSchException ex) {
            System.out.println(ex.getMessage());
            if(ex.getMessage().equals("Auth fail"))
                loginError.setText("Error: Incorrect login credentials");
            else if(ex.getMessage().equals("java.net.ConnectException: Connection refused"))
                loginError.setText("Error: Connection refused");
            else if(ex.getMessage().contains("java.net.UnknownHostException:"))
                loginError.setText("Error: Unable to reach host");
            loginError.setVisible(true);
        }
    }
    
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
